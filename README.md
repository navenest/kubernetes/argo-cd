# argo-cd

## Setup

```bash
kubectl create ns argocd
kubectl apply -n argocd -f manual/secrets.yaml
helm dep up ./clusters/rke2/
helm install argo-cd -n argocd ./clusters/rke2/ -f ./common/values.yaml -f ./clusters/rke2/values.yaml
```